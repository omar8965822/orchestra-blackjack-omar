interface ICardItem {
  code: string;
  image: string;
  value: string;
}

interface ICardData {
  cards: ICardItem[];
  score: number;
}

export type { ICardItem, ICardData };
