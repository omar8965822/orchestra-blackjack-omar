import { useEffect, useState } from "react";

import { getDeck } from "../helpers/getDeck";
import calculateScore from "../helpers/calculateScore";
import { getCards } from "../helpers/getCards";
import { ICardData } from "../utils/types";
import { CardBoard, Deck, Layout } from "../components";

const BlackjackPage = () => {
  const [isLoading, setIsLoading] = useState(false);

  const [userCardsData, setUserCardsData] = useState<ICardData>({
    cards: [],
    score: 0,
  });

  const [computerCardsData, setComputerCardsData] = useState<ICardData>({
    cards: [],
    score: 0,
  });

  const [status, setStatus] = useState("");

  const [deckId, setDeckId] = useState("");
  const [error, setError] = useState("");

  const onClickStartButton = async () => {
    setStatus("");
    setIsLoading(true);
    const deckResponse = await getDeck();
    if (deckResponse) {
      setDeckId(deckResponse);
      const userCardsReponse = await getCards({
        count: 2,
        deckId: deckResponse,
      });

      if (userCardsReponse.length) {
        setUserCardsData({
          cards: userCardsReponse,
          score: calculateScore(userCardsReponse),
        });
      } else {
        setError("There was an error to draw cards");
      }

      const computerCardsReponse = await getCards({
        count: 2,
        deckId: deckResponse,
      });

      if (computerCardsReponse.length) {
        setComputerCardsData({
          cards: computerCardsReponse,
          score: calculateScore(computerCardsReponse),
        });
      } else {
        setError("There was an error to draw cards");
      }

      setIsLoading(false);
    } else {
      setIsLoading(false);
      setError("There was an error to create the game");
    }
  };

  const onClickDrawCard = async () => {
    setIsLoading(true);

    const userCardsReponse = await getCards({
      count: 1,
      deckId: deckId,
    });

    if (userCardsReponse.length) {
      const cards = [...userCardsData.cards, userCardsReponse[0]];
      setUserCardsData({
        cards,
        score: calculateScore(cards),
      });
    } else {
      setError("There was an error to draw cards");
    }

    setIsLoading(false);
  };

  const onClickFinishGame = async () => {
    const flag =
      userCardsData.score <= 21 &&
      userCardsData.score > computerCardsData.score;
    if (flag) {
      setStatus("win");
      setDeckId("");
    } else {
      setStatus("lose");
      setDeckId("");
    }
  };

  useEffect(() => {
    if (userCardsData.score > 21) {
      setStatus("lose");
      setDeckId("");
    }
    if (userCardsData.score === 21) {
      setStatus("win");
      setDeckId("");
    }
  }, [userCardsData.score]);

  return (
    <Layout>
      <CardBoard cards={computerCardsData.cards} type="computer" />

      <Deck
        hasBegun={!(deckId === "")}
        status={status}
        disabled={error !== "" || isLoading}
        computerScore={computerCardsData.score}
        userScore={userCardsData.score}
        onClickButton={deckId === "" ? onClickStartButton : onClickDrawCard}
        onClickFinishGame={onClickFinishGame}
      />

      <CardBoard cards={userCardsData.cards} type="user" />

      <div>{error}</div>
    </Layout>
  );
};

export default BlackjackPage;
