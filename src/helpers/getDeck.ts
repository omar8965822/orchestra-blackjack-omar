import axios from "axios";

interface IDeckResponse {
  deck_id: string;
  remaining: number;
  shuffled: boolean;
  success: boolean;
}

export const getDeck = async () => {
  try {
    const url = `${process.env.REACT_APP_DECK_API}/deck/new/shuffle/?deck_count=1`;
    const { data, status }: { data: IDeckResponse; status: number } =
      await axios.get(url);

    if (status !== 200) {
      // TODO: Handle errors
      return null;
    }

    return data.deck_id;
  } catch (error) {
    // TODO: Handle errors
    return null;
  }
};
