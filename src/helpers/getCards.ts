import axios from "axios";
import { ICardItem } from "../utils/types";

interface IDeckResponse {
  deck_id: string;
  remaining: number;
  shuffled: boolean;
  success: boolean;
  cards: ICardItem[];
}

interface Props {
  count: number;
  deckId: string;
}

export const getCards = async ({ count, deckId }: Props) => {
  try {
    const url = `${process.env.REACT_APP_DECK_API}/deck/${deckId}/draw/?count=${count}`;
    const { data, status }: { data: IDeckResponse; status: number } =
      await axios.get(url);

    if (status === 200) {
      return data.cards;
    } else {
      // TODO: Handle errors
      return [];
    }
  } catch (error) {
    // TODO: Handle errors
    return [];
  }
};
