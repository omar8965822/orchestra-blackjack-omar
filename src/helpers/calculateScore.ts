import { ICardItem } from "../utils/types";

const calculateScore = (cards: ICardItem[]) => {
  let score = 0;

  const aces: ICardItem[] = [];

  cards.forEach((card) => {
    switch (card.value) {
      case "JACK":
      case "QUEEN":
      case "KING":
        score += 10;
        break;
      case "ACE":
        aces.push(card);
        break;
      default:
        score += parseInt(card.value);
        break;
    }
  });

  aces.forEach((aceCard) => {
    if (score > 10) {
      score += 1;
    } else {
      score += 11;
    }
  });

  return score;
};

export default calculateScore;
