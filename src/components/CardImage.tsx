import { FC } from "react";

import CSS from "csstype";

interface Props {
  src: string;
  alt: string;
}

const imageStyle: CSS.Properties = {
  width: "120px",
};

export const CardImage: FC<Props> = ({ src, alt }) => {
  return <img style={imageStyle} alt={alt} className="m-2" src={src} />;
};
