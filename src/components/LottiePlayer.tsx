import { FC } from "react";

import { useLottie } from "lottie-react";

import winner from "./lotties/winner.json";
import lose from "./lotties/lose.json";

interface Props {
  type: string;
}
const LottiePlayer: FC<Props> = ({ type }) => {
  const options = {
    animationData: type === "win" ? winner : lose,
    loop: true,
  };

  const { View } = useLottie(options, { width: "auto", height: "30vh" });

  return <>{View}</>;
};

export default LottiePlayer;
