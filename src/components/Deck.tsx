import { FC } from "react";
import CSS from "csstype";
import { Button, Col, Container, Row } from "react-bootstrap";
import { CardImage } from "./CardImage";

import LottiePlayer from "./LottiePlayer";

interface Props {
  status: string;
  disabled: boolean;
  hasBegun: boolean;
  computerScore: number;
  userScore: number;
  onClickButton: () => {};
  onClickFinishGame: () => {};
}

const container: CSS.Properties = {
  minHeight: "30vh",
};

export const Deck: FC<Props> = ({
  status,
  disabled,
  hasBegun,
  onClickButton,
  onClickFinishGame,
  computerScore,
  userScore,
}) => {
  return (
    <div style={container}>
      {status === "" ? (
        <Container>
          <Row className="justify-content-center align-items-center">
            <Col className="text-center">
              <Container>
                <CardImage src={`/images/robot.png`} alt="Computer Avatar" />
              </Container>

              <Container className="text-center">
                Computer score
                <span className="mx-2 p-1 border rounded">{computerScore}</span>
              </Container>
            </Col>
            <Col className="text-center">
              <Container>
                <CardImage
                  alt="back card"
                  src="https://deckofcardsapi.com/static/img/back.png"
                />
              </Container>
            </Col>

            <Col className="text-center">
              <Container>
                <CardImage src={`/images/user.png`} alt="User Avatar" />
              </Container>

              <Container className="text-center">
                User score
                <span className="mx-2 p-1 border rounded">{userScore}</span>
              </Container>
            </Col>
          </Row>
          <Row className="justify-content-center">
            {hasBegun ? (
              <>
                <Col md="auto">
                  <Button disabled={disabled} onClick={onClickButton}>
                    Draw a card
                  </Button>
                </Col>
                <Col md="auto">
                  <Button
                    variant="success"
                    disabled={disabled}
                    onClick={onClickFinishGame}
                  >
                    Finish game
                  </Button>
                </Col>
              </>
            ) : (
              <Col md="auto">
                <Button disabled={disabled} onClick={onClickButton}>
                  Start game
                </Button>
              </Col>
            )}
          </Row>
        </Container>
      ) : (
        <>
          <div>
            <LottiePlayer type={status} />
          </div>
          <div className="d-flex justify-content-center mt-2">
            <Button disabled={disabled} onClick={onClickButton}>
              Start a new game
            </Button>
          </div>
        </>
      )}
    </div>
  );
};
