import { FC } from "react";

import { Container } from "react-bootstrap";
import CSS from "csstype";

import { ICardItem } from "../utils/types";
import { CardImage } from "./CardImage";

interface Props {
  cards: ICardItem[];
  type: string;
}

const boardContainer: CSS.Properties = {
  minHeight: "25vh",
};

export const CardBoard: FC<Props> = ({ cards, type }) => {
  return (
    <div style={boardContainer}>
      <h1>{type === "user" ? "My cards" : "Computer cards"}</h1>
      <Container className="m-2 d-flex">
        {cards.map((card) => (
          <CardImage src={card.image} alt={card.code} key={card.code} />
        ))}
      </Container>
    </div>
  );
};
