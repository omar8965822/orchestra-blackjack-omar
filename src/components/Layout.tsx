import { FC } from "react";

interface Props {
  children: React.ReactNode;
}

export const Layout: FC<Props> = ({ children }) => {
  return <main className="p-4">{children}</main>;
};
